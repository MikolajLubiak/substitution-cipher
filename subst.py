#!/usr/bin/env python3.10

from string import ascii_lowercase as lc, ascii_uppercase as uc
from sys import argv
from random import sample

def genKey(s: str):
    index = tuple(sample(range(len(s)), len(s)))
    key = ""
    for i in range(len(s)):
        key+=s[index[i]]
    return key

def subst(m: str, s: str):
    return "".join([s[(lc+uc).find(a)] if a in (lc+uc) else a for a in m])

if __name__ == "__main__":
    try:
        a = argv[1]
    except IndexError:
        a = input("Text: ")
    try:
        b = argv[2]
    except IndexError:
        b = input("Key (Leave empty if non): ")
    if not b:
        b = genKey(lc+uc)
        print(f"Key: {b}")
    print(subst(a, b))
